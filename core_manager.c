#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MCC 255 //Maximum Core Count
void print_help(){
	fprintf(stdout, "This program can turn on and off the cores in your Linux system.\nIf you run the program without any flags it will show you a flag of all available cores and their stauts. Otherwise there are several flags you need to consider for taking use of the program.\nU: Turns on all disabled cores.\nD: Turns off all enabled cores.\nu: Turns on a specific core if disabled.\nd: Turns off a specific core if enabled.\n\nExample of usage:\n\tcore_manager -U \nor\n\tcore_manager -d 5\nIf for example, a core is enabled and the U or u flag is set, nothing will happen to the core.\nSince all core states are stored seperately in an array and the program will check if the cores need to be turned on or off.\n");
}

unsigned char count_cores(){
	FILE *fp;
	unsigned char core_count_max = 255;
	unsigned char core_count = 0;
	size_t size_cpu_path = sizeof("/sys/devices/system/cpu/cpu012");
	char mod_cpu_path[size_cpu_path];	

	for(unsigned char i = 1; i < core_count_max; i++){
		snprintf(mod_cpu_path, size_cpu_path, "/sys/devices/system/cpu/cpu%i", i);
		if( (fp = fopen(mod_cpu_path,"r")) == NULL){
			core_count = i; 
			break;
		}
		else{fclose(fp);}
	}
	fprintf(stdout,"System cores: %d\n",core_count);
	return core_count;
}
void cpu_tab(unsigned char core_count, unsigned char cpu_status[]){
	fprintf(stdout, "|\t");
	for(char i = 1; i < core_count; i++){
		fprintf(stdout, " %d:", i);
		if(cpu_status[i] == 0){fprintf(stdout, " Offline\t|\t");}
		else{fprintf(stdout, " Online\t|\t");}
	}
	fprintf(stdout, "\n");
}
void check_cpu_state(unsigned char core_count, unsigned char cpu_status[]){
	FILE *fp;
	char read_buffer[2] = {0};
	size_t size_cpu_online_path = sizeof("/sys/devices/system/cpu/cpu012/online");
	char mod_cpu_online_path[size_cpu_online_path];	
	unsigned int core_status;
	for(unsigned char i = 1; i < core_count; i++){

		snprintf(mod_cpu_online_path, size_cpu_online_path, "/sys/devices/system/cpu/cpu%i/online", i);
		//fprintf(stdout, mod_cpu_online_path);fprintf(stdout,"\n");
	
		if( (fp = fopen(mod_cpu_online_path,"r")) != NULL){

			fread(read_buffer,1,1,fp);
			fclose(fp);
			core_status = atoi(read_buffer);

			if( core_status == 0 ){ cpu_status[i] = core_status;}
			else{ cpu_status[i] = core_status;}
		}
		else{fprintf(stderr, "Something went wrong with opening the online file.\n"); exit(-1);}
	}
	cpu_tab(core_count, cpu_status);
	//fprintf(stdout, "Checking cores finished.\n");
}

void change_cpu_state(unsigned char core_number, char onoff){
	
	FILE *fp;
	
	size_t size_cpu_online_path = sizeof("/sys/devices/system/cpu/cpu012/online");
	char mod_cpu_online_path[size_cpu_online_path];
	snprintf(mod_cpu_online_path, size_cpu_online_path, "/sys/devices/system/cpu/cpu%i/online", core_number);
	
	if( (fp = fopen(mod_cpu_online_path, "w")) != NULL){	
		switch(onoff){
			case 0: fwrite("0",1,1,fp);break;
			case 1:	fwrite("1",1,1,fp);break;
			default:	exit(-1);break;
		}
		fclose(fp);
		
	}
	else{fprintf(stderr, "Something went wrong with opening the online file\n"); exit(-1);}
}

void turn_on_all_cores(char core_count, unsigned char cpu_status[]){
	for(unsigned char i = 1; i < core_count; i++){
		if(cpu_status[i] == 0){
			change_cpu_state(i, 1);
		}
	}
}

void turn_off_all_cores(char core_count, unsigned char cpu_status[]){
	for(unsigned char i = 1; i < core_count; i++){
		if(cpu_status[i] == 1){
			change_cpu_state(i, 0);
		}
	}
}

int main(int argc, char *argv[]){

	//CLI
	int8_t option = 0;
	char flags[] = "UDu:d:h";

	unsigned char c_flag = 0; //Specifies wich core should be turned on or off
	unsigned char up_all_flag = 0; //Specifies the case where all cores should be turned on
	unsigned char down_all_flag = 0; // "" turned off.
	unsigned int up_flag = 0; //Forces a 1 into the "online" file, but basically just turning the core on. Instead of flipping the value
	unsigned int down_flag = 0; //Forces a 0 """" 
	unsigned int check_operation_flags = 0;
	unsigned int check_updown_flags = 0;
	unsigned int check_all_updown_flags = 0;
	unsigned int core_number = 0; 

	if(argc > 1){
		while((option = getopt(argc, argv, flags)) != -1){
			switch(option){
				case 'U':// Turns on all cores, if disabled
					up_all_flag = 1;
					break;
				case 'D':// Turns down all cores, if enabled
					down_all_flag = 1;
					break;
				case 'u':// Turns up a specific core, if disabled
					up_flag = 1;
					if(atoi(optarg) > 0){core_number = atoi(optarg);}
					else{fprintf(stderr, "This is not a valid core number!"); exit(-1);}
					break;
				case 'd':// Turns down a specific core, if enabled
					down_flag = 1;
					if(atoi(optarg) > 0){core_number = atoi(optarg);}
					else{fprintf(stderr, "This is not a valid core number!"); exit(-1);}
					break;
				case 'h':
					print_help();
					return 0;
				case '?':
					exit(-1);
			}
		}
	}
		
	check_operation_flags = up_flag+down_flag+up_all_flag+down_all_flag;
	if(check_operation_flags > 1){fprintf(stderr, "You are only allowed to parse one operation at one run!"); exit(-1);}
	
	unsigned char cpu_status[255] = {0}; // All CPU states get stored in here. With simply Online (1) and Offline (0)
	unsigned char active_core_sum = 0;
	int core_count = 0;
	
	//for(unsigned char i = 1; i < 255; i++){
	//	active_core_sum = active_core_sum+cpu_status[i];
	//}
	
	
	if(up_flag == 1){
		core_count = count_cores();
		check_cpu_state(core_count, cpu_status);
		if(cpu_status[core_number] == 0){
			change_cpu_state(core_number, 1);
		}
		check_cpu_state(core_count, cpu_status);
	}
	else if(down_flag == 1){
		core_count = count_cores();
		check_cpu_state(core_count, cpu_status);
		if(cpu_status[core_number] == 1){
			change_cpu_state(core_number, 0);
		}
		check_cpu_state(core_count, cpu_status);
	}
	else if(up_all_flag == 1){
		core_count = count_cores();
		check_cpu_state(core_count, cpu_status);
		turn_on_all_cores(core_count, cpu_status);
		check_cpu_state(core_count, cpu_status);
	}
	else if(down_all_flag == 1){
		core_count = count_cores();
		check_cpu_state(core_count, cpu_status);
		turn_off_all_cores(core_count, cpu_status);
		check_cpu_state(core_count, cpu_status);
	}
	else{
		check_cpu_state(core_count, cpu_status);
	}
	return 0;
}
