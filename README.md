# core_manager

## Installation
Currently, this program is only working under linux where the standard path fore cpu cores is "/sys/devices/system/cpu/".
## Usage
The program is using the "u", "d", "U", "D" -flags.
-u : Forces the program to turn on the specified cores.
-d : Forces the prorgam to turn off the specified cores.
-U : All turned off cores will be turned on with that flag.
-D : "" on "" turned off "".
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## License
This project is under the GNU AGPLv3 License.

## Project Status
Working on improvements.

